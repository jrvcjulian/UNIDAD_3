                           PATRONES DE DICE�O
FACTORY
Uno de los patrones de dise�o m�s utilizados en Java es el patr�n Factory que es un patr�n de dise�o creacional y que sirve para construir una jerarqu�a de clases.
BUILDER
Este nos ayudara b�sicamente para las clases especializadas en construir instancias de otra clase que podemos hacer usable con una API fluida y alguna cosa m�s deseable que explico en el art�culo.
PROXY
El patr�n proxy trata de proporcionar un objeto intermediario que represente o sustituya al objeto original con motivo de controlar el acceso y otras caracter�sticas del mismo.
Se aplica cuando:
El patr�n Proxy se usa cuando se necesita una referencia a un objeto m�s flexible o sofisticada que un puntero. Seg�n la funcionalidad requerida podemos encontrar varias aplicaciones:
Proxy remoto: representa un objeto en otro espacio de direcciones.
Proxy virtual: retrasa la creaci�n de objetos costosos.
Proxy de protecci�n: controla el acceso a un objeto.
Referencia inteligente: tiene la misma finalidad que un puntero pero adem�s ejecuta acciones adicionales sobre el objeto, como por ejemplo el control de concurrencia.
SINGLETON
Este sirve para base de datos, utilizarlo es buena para cuando solo existe un m�todo en el cual  se retornen todas las conexiones o en juegos para mantener la cantidad de puntos independientemente la cantidad de juegos que se halla llamado.
Pruebas unitarias


******Patron de dise�o que se aplicara: 

El Patr�n Singleton tambi�n se conoce como Instancia �nica, su objetivo es restringir la creaci�n de objetos  pertenecientes a una clase, de modo que solo se tenga una �nica instancia de la clase para toda la aplicaci�n, garantizando as� un punto de acceso global al objeto creado. 

Para implementarlo, la clase Singleton debe tener un constructor privado que solo sera accedido desde la misma clase, se crea tambi�n una instancia privada de la clase, as� como un m�todo est�tico que permita el acceso a dicha instancia de la forma ClaseSingleton.getInstanciaSingleton();


******Pruebas unitarias con JUnit.

Vamos a utilizar JUnit para probar los m�todos. Para ello deberemos crear una serie de clases en las que implementaremos las pruebas dise�adas. Esta implementaci�n consistir� b�sicamente en invocar el m�todo que est� siendo probado pas�ndole los par�metros de entrada establecidos para cada caso de prueba, y comprobar si la salida real coincide con la salida esperada. Esto en principio lo podr�amos hacer sin necesidad de utilizar JUnit, pero el utilizar esta herramienta nos va a ser de gran utilidad ya que nos proporciona un framework que nos obligar� a implementar las pruebas en un formato est�ndar que podr� ser reutilizable y entendible por cualquiera que conozca la librer�a.